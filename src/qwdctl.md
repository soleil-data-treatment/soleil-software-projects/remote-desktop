% QWDCTL(1)
% Roland Mas
% April 2021

# NAME

qwdctl - Control qemu-web-desktop

# SYNOPSIS

**qwdctl** --edit | --download | --start VM | --status | ...

# DESCRIPTION

**qwdctl** is a simple script that downloads virtual machine images
for use with **qemu-web-desktop** (aka DARTS) and/or refreshes the list of
available images so that **qemu-web-desktop** can display it in its
web interface.

In addition, the status of the running sessions can be displayed
and it is possible to start virtual machines manually and display
them in a browser.

Supported virtual machine formats are: ISO, QCOW2, VDI, VMDK, RAW, VHD/VHDX, QED

Each entry in the configuration file `/etc/qemu-web-desktop/machines.conf` 
spans on 3 lines:

-  [name.ext] 
-  url=[URL to virtual machine disk, optional]
-  description=[description to be shown in the service page] 

Images listed in the configuration file without a `url=` parameter are
expected to be downloaded by hand and installed into
`/var/lib/qemu-web-desktop/machines` by the local administrator. Then, just 
specify the [name.ext] and description.

# SUBCOMMANDS

**--download|download|update**
:   scan the /etc/qemu-web-desktop/machines.conf file for [name.ext] and download 
    them when URL are given. A 'refresh' is then performed. 
    Virtual machine images are stored into `/var/lib/qemu-web-desktop/machines`.

**--refresh|refresh**
:   scan the `/etc/qemu-web-desktop/machines.conf` file, and generate the 
    `/var/lib/qemu-web-desktop/machines.html` that lists available images to show 
    in the qemu-web-desktop main form.

**--status|status**
:   list running sessions.

**--start|start VM ...**
:   start the given VM or ISO file (full path) in a browser. Connect to it with
    the displayed URL. Further arguments are passed to the service (see config.pl)
    e.g. `--snapshot_alloc_mem=1024` (in GB) and `--snapshot_alloc_cpu=2`.
    Changes are lost except when specifying option `--snapshot_use_master=1`,
    requiring write access. When running ISO's you may also specify
    `--snapshot_alloc_disk=40` (in GB).

**--stop|stop TOKEN**
:   stop sessions matching TOKEN. Some snapshot files may be left-over.
 
**--gpu|gpu VENDOR:MODEL**
:   configure GPU for pass-through. The GPU_ids are e.g. `10de:1d01`
 
**--gpu_unlock|gpu_unlock**
:   unlock/re-attach all GPU's to server (uninstall pass-through).
 
**--edit|edit [machines|config|web]**
:   edit the VM/machine list, the service configuration file or the service web page.
    In the case of the VM list, the `qwdctl download` command is triggered 
    automatically after edit.

**--version|version|-v**
:   show qemu-web-desktop version

**--help|help|-h**
:   show this help

# FILES

- /etc/qemu-web-desktop/config.pl
- /etc/qemu-web-desktop/machines.conf
- /var/lib/qemu-web-desktop/machines.html
- /var/lib/qemu-web-desktop/machines
- /usr/share/qemu-web-desktop/html/desktop
- https://gitlab.com/soleil-data-treatment/soleil-software-projects/qemu-web-desktop
